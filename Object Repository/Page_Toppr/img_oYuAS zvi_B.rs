<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_oYuAS zvi_B</name>
   <tag></tag>
   <elementGuidId>f22f6c9b-af26-46bd-bb2b-b1d1e9be1f53</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://d3jlfsfsyc6yvi.cloudfront.net/image/w:128/h:128/c:g_center/f:jpg/q:90/https%3A%2F%2Fhaygot.s3.amazonaws.com%2Fanon-avatars%2F47.jpg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>srcset</name>
      <type>Main</type>
      <value>https://d3jlfsfsyc6yvi.cloudfront.net/image/w:128/h:128/c:g_center/f:jpg/q:90/https%3A%2F%2Fhaygot.s3.amazonaws.com%2Fanon-avatars%2F47.jpg 1x, https://d3jlfsfsyc6yvi.cloudfront.net/image/w:128/h:128/c:g_center/f:jpg/q:90/https%3A%2F%2Fhaygot.s3.amazonaws.com%2Fanon-avatars%2F47.jpg 2x</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oYuAS zvi_B</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;LCw6b&quot;]/div[@class=&quot;_2dBhi AaLIp&quot;]/div[@class=&quot;_3WsER&quot;]/div[@class=&quot;UOHf5 utils_spacer&quot;]/div[@class=&quot;_3_27H flex_alignMiddle flex_alignRight&quot;]/div[@class=&quot;_2oPWt&quot;]/div[@class=&quot;_1SWJw&quot;]/img[@class=&quot;oYuAS zvi_B&quot;]</value>
   </webElementProperties>
</WebElementEntity>
