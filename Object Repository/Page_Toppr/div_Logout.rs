<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Logout</name>
   <tag></tag>
   <elementGuidId>55ff1f81-bf78-4815-a543-8b2f80868ad8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>_23gZp flex_alignMiddle</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;LCw6b&quot;]/div[@class=&quot;_2dBhi AaLIp&quot;]/div[@class=&quot;_3WsER&quot;]/div[@class=&quot;UOHf5 utils_spacer&quot;]/div[@class=&quot;_3_27H flex_alignMiddle flex_alignRight&quot;]/div[@class=&quot;_2oPWt&quot;]/div[@class=&quot;_1SWJw d6FMl&quot;]/div[@class=&quot;HcleS&quot;]/div[@class=&quot;_23MUp&quot;]/div[@class=&quot;_2Cg0v&quot;]/div[@class=&quot;_3nAYv&quot;]/a[5]/div[@class=&quot;_23gZp flex_alignMiddle&quot;]</value>
   </webElementProperties>
</WebElementEntity>
